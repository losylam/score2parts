
\version "2.14.2"
% automatically converted from orig_flute.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPOneVoiceOne =  \relative a'' {
    \clef "treble" \key bes \major \time 7/8 \repeat volta 2 {
        | % 1
        R2..*4 }
    \repeat volta 2 {
        | % 5
        \mark \markup { \box { A } } | % 5
        a8 ^"" [ bes8 a8 ] c4 bes8 [ a8 ] | % 6
        g8 [ es8 g8 ] fis4 fis4 | % 7
        a8 [ bes8 a8 ] c4 es8 [ d8 ] | % 8
        c8 [ bes8 d8 ] c4 c4 | % 9
        a8 [ bes8 a8 ] c4 bes8 [ a8 ] | \barNumberCheck #10
        g8 [ es8 g8 ] fis4 fis4 | % 11
        a8 ( -> [ bes8 a8 ) c8 ( -> bes8 ) a8 ( -> g8 ) ] }
    \alternative { {
            | % 12
            fis8 [ a8 g8 ] fis2 }
        {
            | % 13
            bes8 [ a8 g8 ] fis4 a4 }
        } \repeat volta 2 {
        | % 14
        \mark \markup { \box { B } } | % 14
        bes8 [ d8 c8 ] bes4 d8 [ c8 ] | % 15
        a8 [ c8 bes8 ] a4 bes8 [ a8 ] | % 16
        g8 [ a8 bes8 d8 c8 bes8 g8 ] | % 17
        fis8 [ g8 a8 ] c4 d8 [ c8 ] | % 18
        bes8 [ d8 c8 ] bes4 d8 [ c8 ] | % 19
        a8 [ c8 bes8 ] a4 bes8 [ a8 ] | \barNumberCheck #20
        g8 [ a8 bes8 d8 c8 bes8 g8 ] }
    \alternative { {
            | % 21
            fis4. ~ fis4 a4 }
        {
            | % 22
            fis4. ~ fis2 }
        } \repeat volta 2 {
        | % 23
        \mark \markup { \box { Pont } } | % 23
        R2..*2 }
    \repeat volta 2 {
        | % 25
        \mark \markup { \box { C } } | % 25
        es'8 ^"x1 : Trp Solo x2 : Sax + Cl x3 : Trp + Fl" [ d8 c8 ] bes4
        d8 [ c8 ] | % 26
        bes8 [ c8 bes8 ] a4 g8 [ ges8 ] | % 27
        g8 [ a8 bes8 cis8 d8 e8 fis8 ] | % 28
        g8 [ es8 g8 ] fis4 fis4 | % 29
        es8 [ d8 c8 ] bes4 d8 [ c8 ] | \barNumberCheck #30
        bes8 [ c8 bes8 ] a4 g8 [ fis8 ] }
    \alternative { {
            | % 31
            g8 [ a8 bes8 c8 d8 c8 bes8 ] | % 32
            g8 [ a8 g8 ] fis2 }
        {
            | % 33
            g8 ^"Tutti" [ a8 bes8 c8 d8 c8 bes8 ] | % 34
            g8 [ a8 g8 ] fis4 a4 }
        } \repeat volta 2 {
        | % 35
        \mark \markup { \box { B } } | % 35
        bes8 [ d8 c8 ] bes4 d8 [ c8 ] | % 36
        a8 [ c8 bes8 ] a4 bes8 [ a8 ] | % 37
        g8 [ a8 bes8 d8 c8 bes8 g8 ] | % 38
        fis8 [ g8 a8 ] c4 d8 [ c8 ] | % 39
        bes8 [ d8 c8 ] bes4 d8 [ c8 ] | \barNumberCheck #40
        a8 [ c8 bes8 ] a4 bes8 [ a8 ] | % 41
        g8 [ a8 bes8 d8 c8 bes8 g8 ] }
    \alternative { {
            | % 42
            fis4. ~ fis4 a4 }
        {
            | % 43
            fis4. ~ fis2 }
        } \repeat volta 2 {
        | % 44
        \mark \markup { \box { Transition } } | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            R2.. }
        } \repeat volta 2 {
        | % 49
        R2..*4 }
    \repeat volta 2 {
        | % 53
        \mark \markup { \box { Chorus } } | % 53
        R2..*4 }
    \repeat volta 2 {
        | % 57
        \mark \markup { \box { A } } | % 57
        a8 [ bes8 a8 ] c4 bes8 [ a8 ] | % 58
        g8 [ es8 g8 ] fis4 fis4 | % 59
        a8 [ bes8 a8 ] c4 es8 [ d8 ] | \barNumberCheck #60
        c8 [ bes8 d8 ] c4 c4 | % 61
        a8 [ bes8 a8 ] c4 bes8 [ a8 ] | % 62
        g8 [ es8 g8 ] fis4 fis4 | % 63
        a8 [ bes8 a8 c8 bes8 a8 g8 ] }
    \alternative { {
            | % 64
            fis8 [ a8 g8 ] fis2 }
        {
            | % 65
            bes8 [ a8 g8 ] fis4 a4 }
        } \repeat volta 2 {
        | % 66
        \mark \markup { \box { B' } } | % 66
        bes8 [ d8 c8 ] bes4 d8 [ c8 ] | % 67
        a8 [ c8 bes8 ] a4 bes8 [ a8 ] | % 68
        g8 [ a8 bes8 d8 c8 bes8 g8 ] }
    \alternative { {
            | % 69
            fis8 [ g8 a8 ] c4 d8 [ c8 ] }
        {
            | \barNumberCheck #70
            fis,4. ~ fis4 c'4 }
        } \repeat volta 2 {
        | % 71
        \mark \markup { \box { B'' } } | % 71
        d4. ~ d2 | % 72
        c4. ~ c2 | % 73
        bes4. ~ bes2 }
    \alternative { {
            | % 74
            a4 ^"" a8 bes4 c4 }
        {
            | % 75
            a4. a4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Flute"
    \set Staff.shortInstrumentName = "Fl."
    \context Staff << 
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
        >>
    >>

