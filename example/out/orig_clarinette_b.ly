
\version "2.14.2"
% automatically converted from orig_clarinette_b.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPTwoVoiceOne =  \relative b' {
    \transposition bes \clef "treble" \key c \major \time 7/8 \repeat
    volta 2 {
        R2..*4 }
    \repeat volta 2 {
        | % 5
        b8 [ c8 b8 ] d4 c8 [ b8 ] | % 6
        a8 [ f8 a8 ] gis4 gis4 | % 7
        b8 [ c8 b8 ] d4 f8 [ e8 ] | % 8
        d8 [ c8 e8 ] d4 d4 | % 9
        b8 [ c8 b8 ] d4 c8 [ b8 ] | \barNumberCheck #10
        a8 [ f8 a8 ] gis4 gis4 | % 11
        b8 ( -> [ c8 b8 ) d8 ( -> c8 ) b8 ( -> a8 ) ] }
    \alternative { {
            | % 12
            gis8 [ b8 a8 ] gis2 }
        {
            | % 13
            c8 [ b8 a8 ] gis4 b4 }
        } \repeat volta 2 {
        | % 14
        c8 [ e8 d8 ] c4 e8 [ d8 ] | % 15
        b8 [ d8 c8 ] b4 c8 [ b8 ] | % 16
        a8 [ b8 c8 e8 d8 c8 a8 ] | % 17
        gis8 [ a8 b8 ] d4 e8 [ d8 ] | % 18
        c8 [ e8 d8 ] c4 e8 [ d8 ] | % 19
        b8 [ d8 c8 ] b4 c8 [ b8 ] | \barNumberCheck #20
        a8 [ b8 c8 e8 d8 c8 a8 ] }
    \alternative { {
            | % 21
            gis4. ~ gis4 b4 }
        {
            | % 22
            gis4. ~ gis2 }
        } \repeat volta 2 {
        | % 23
        R2..*2 }
    \repeat volta 2 {
        | % 25
        f'8 [ e8 d8 ] c4 e8 [ d8 ] | % 26
        c8 [ d8 c8 ] b4 a8 [ as8 ] | % 27
        a8 [ b8 c8 dis8 e8 fis8 gis8 ] | % 28
        a8 [ f8 a8 ] gis4 gis4 | % 29
        f8 [ e8 d8 ] c4 e8 [ d8 ] | \barNumberCheck #30
        c8 [ d8 c8 ] b4 a8 [ gis8 ] }
    \alternative { {
            | % 31
            a8 [ b8 c8 d8 e8 d8 c8 ] | % 32
            a8 [ b8 a8 ] gis2 }
        {
            | % 33
            a8 [ b8 c8 d8 e8 d8 c8 ] | % 34
            a8 [ b8 a8 ] gis4 b4 }
        } \repeat volta 2 {
        | % 35
        c8 [ e8 d8 ] c4 e8 [ d8 ] | % 36
        b8 [ d8 c8 ] b4 c8 [ b8 ] | % 37
        a8 [ b8 c8 e8 d8 c8 a8 ] | % 38
        gis8 [ a8 b8 ] d4 e8 [ d8 ] | % 39
        c8 [ e8 d8 ] c4 e8 [ d8 ] | \barNumberCheck #40
        b8 [ d8 c8 ] b4 c8 [ b8 ] | % 41
        a8 [ b8 c8 e8 d8 c8 a8 ] }
    \alternative { {
            | % 42
            gis4. ~ gis4 b4 }
        {
            | % 43
            gis4. ~ gis2 }
        } \repeat volta 2 {
        | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            R2.. }
        } \repeat volta 2 {
        | % 49
        R2..*4 }
    \repeat volta 2 {
        | % 53
        R2..*4 }
    \repeat volta 2 {
        | % 57
        b8 [ c8 b8 ] d4 c8 [ b8 ] | % 58
        a8 [ f8 a8 ] gis4 gis4 | % 59
        b8 [ c8 b8 ] d4 f8 [ e8 ] | \barNumberCheck #60
        d8 [ c8 e8 ] d4 d4 | % 61
        b8 [ c8 b8 ] d4 c8 [ b8 ] | % 62
        a8 [ f8 a8 ] gis4 gis4 | % 63
        b8 [ c8 b8 d8 c8 b8 a8 ] }
    \alternative { {
            | % 64
            gis8 [ b8 a8 ] gis2 }
        {
            | % 65
            c8 [ b8 a8 ] gis4 b4 }
        } \repeat volta 2 {
        | % 66
        c8 [ e8 d8 ] c4 e8 [ d8 ] | % 67
        b8 [ d8 c8 ] b4 c8 [ b8 ] | % 68
        a8 [ b8 c8 e8 d8 c8 a8 ] }
    \alternative { {
            | % 69
            gis8 [ a8 b8 ] d4 e8 [ d8 ] }
        {
            | \barNumberCheck #70
            gis,4. ~ gis4 d'4 }
        } \repeat volta 2 {
        | % 71
        e4. ~ e2 | % 72
        d4. ~ d2 | % 73
        c4. ~ c2 }
    \alternative { {
            | % 74
            gis4 gis8 a4 b4 }
        {
            | % 75
            gis4. gis4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Clarinette B"
    \set Staff.shortInstrumentName = "Cl. B"
    \context Staff << 
        \context Voice = "PartPTwoVoiceOne" { \PartPTwoVoiceOne }
        >>
    >>

