
\version "2.14.2"
% automatically converted from orig_trombone.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPSixVoiceOne =  \relative fis {
    \clef "bass" \key bes \major \time 7/8 \repeat volta 2 {
        | % 1
        <fis a>4 ^"x1 Tacet" <fis a>8 -. <fis a>4 <fis a>4 | % 2
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 3
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 4
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
    \repeat volta 2 {
        | % 5
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 6
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 7
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 8
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 9
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. |
        \barNumberCheck #10
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 11
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
    \alternative { {
            | % 12
            <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. }
        {
            | % 13
            <es g>4 -. <es g>8 -. <fis a>4 <a c>4 }
        } \repeat volta 2 {
        | % 14
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. | % 15
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 16
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. | % 17
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 18
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. | % 19
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | \barNumberCheck #20
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. }
    \alternative { {
            | % 21
            <fis a>4 -. <fis a>8 -. <fis a>4 <a c>4 }
        {
            | % 22
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
        } \repeat volta 2 {
        | % 23
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 24
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. }
    \repeat volta 2 {
        | % 25
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 26
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 27
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 28
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 29
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. |
        \barNumberCheck #30
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. }
    \alternative { {
            | % 31
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 32
            <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. }
        {
            | % 33
            <es g>4 -. r8 r2 | % 34
            r2 r8 <a c>4 }
        } \repeat volta 2 {
        | % 35
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. | % 36
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 37
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. | % 38
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 39
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. |
        \barNumberCheck #40
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 41
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. }
    \alternative { {
            | % 42
            <fis a>4 -. <fis a>8 -. <fis a>4 <a c>4 }
        {
            | % 43
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
        } \repeat volta 2 {
        | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            <c' es>8 [ <bes d>8 <a c>8 <g bes>8 <fis a>8 <es g>8 <c es>8
            ] }
        } \repeat volta 2 {
        | % 49
        <d fis>4 <d fis>8 <fis a>4 <a c>4 | \barNumberCheck #50
        <es g>4. <c es>4 <c es>8 [ <cis g'>8 ] | % 51
        <d fis>4 <d fis>8 <fis a>4 <a c>4 | % 52
        <c es>8 [ <bes d>8 <a c>8 <g bes>8 <fis a>8 <es g>8 <c es>8 ] }
    \repeat volta 2 {
        | % 53
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 54
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 55
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 56
        <fis a>4 -. <d fis>8 <es g>4 <f as>4 }
    \repeat volta 2 {
        | % 57
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 58
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 59
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. |
        \barNumberCheck #60
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 61
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. | % 62
        <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. | % 63
        <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
    \alternative { {
            | % 64
            <es g>4 -. <es g>8 -. <fis a>4 -. <fis a>4 -. }
        {
            | % 65
            <es g>4 -. <es g>8 -. <fis a>4 <a c>4 }
        } \repeat volta 2 {
        | % 66
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. | % 67
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 68
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. }
    \alternative { {
            | % 69
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
        {
            | \barNumberCheck #70
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <a c>4 }
        } \repeat volta 2 {
        | % 71
        <bes d>4 -. <bes d>8 -. <bes d>4 -. <bes d>4 -. | % 72
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 73
        <g bes>4 -. <g bes>8 -. <g bes>4 -. <g bes>4 -. }
    \alternative { {
            | % 74
            <fis a>4 -. <fis a>8 -. <fis a>4 -. <fis a>4 -. }
        {
            | % 75
            <fis a>4 -. <fis a>8 -. <fis a>4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Trombone"
    \set Staff.shortInstrumentName = "Trb."
    \context Staff << 
        \context Voice = "PartPSixVoiceOne" { \PartPSixVoiceOne }
        >>
    >>

