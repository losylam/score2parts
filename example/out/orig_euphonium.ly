
\version "2.14.2"
% automatically converted from orig_euphonium.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPSevenVoiceOne =  \relative gis' {
    \transposition bes, \key c \major \time 7/8 \repeat volta 2 {
        | % 1
        <gis b>4 ^"x1 Tacet" <gis b>8 -. <gis b>4 <gis b>4 | % 2
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 3
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 4
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
    \repeat volta 2 {
        | % 5
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 6
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 7
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 8
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 9
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. |
        \barNumberCheck #10
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 11
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
    \alternative { {
            | % 12
            <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. }
        {
            | % 13
            <f a>4 -. <f a>8 -. <gis b>4 <b d>4 }
        } \repeat volta 2 {
        | % 14
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | % 15
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | % 16
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 17
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 18
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | % 19
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | \barNumberCheck #20
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. }
    \alternative { {
            | % 21
            <gis b>4 -. <gis b>8 -. <gis b>4 <b d>4 }
        {
            | % 22
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
        } \repeat volta 2 {
        | % 23
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 24
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. }
    \repeat volta 2 {
        | % 25
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 26
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 27
        <gis b>4 -. <a b>8 -. <gis b>4 -. <gis b>4 -. | % 28
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 29
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. |
        \barNumberCheck #30
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. }
    \alternative { {
            | % 31
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 32
            <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. }
        {
            | % 33
            <f a>4 -. r8 r2 | % 34
            r2 r8 <b d>4 }
        } \repeat volta 2 {
        | % 35
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | % 36
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | % 37
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. | % 38
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 39
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | \barNumberCheck #40
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | % 41
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. }
    \alternative { {
            | % 42
            <gis b>4 -. <gis b>8 -. <gis b>4 <b d>4 }
        {
            | % 43
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
        } \repeat volta 2 {
        | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            <d' f>8 [ <c e>8 <b d>8 <a c>8 <gis b>8 <f a>8 <d f>8 ] }
        } \repeat volta 2 {
        | % 49
        <e gis>4 <e gis>8 <gis b>4 <b d>4 | \barNumberCheck #50
        <f a>4. <d f>4 <d f>8 [ <dis a'>8 ] | % 51
        <e gis>4 <e gis>8 <gis b>4 <b d>4 | % 52
        <d f>8 [ <c e>8 <b d>8 <a c>8 <gis b>8 <f a>8 <d f>8 ] }
    \repeat volta 2 {
        | % 53
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 54
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 55
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 56
        <gis b>4 -. <e gis>8 <f a>4 <g bes>4 }
    \repeat volta 2 {
        | % 57
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 58
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 59
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. |
        \barNumberCheck #60
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 61
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. | % 62
        <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. | % 63
        <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
    \alternative { {
            | % 64
            <f a>4 -. <f a>8 -. <gis b>4 -. <gis b>4 -. }
        {
            | % 65
            <f a>4 -. <f a>8 -. <gis b>4 <b d>4 }
        } \repeat volta 2 {
        | % 66
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | % 67
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | % 68
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. }
    \alternative { {
            | % 69
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
        {
            | \barNumberCheck #70
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <b d>4 }
        } \repeat volta 2 {
        | % 71
        <c e>4 -. <c e>8 -. <c e>4 -. <c e>4 -. | % 72
        <b d>4 -. <b d>8 -. <b d>4 -. <b d>4 -. | % 73
        <a c>4 -. <a c>8 -. <a c>4 -. <a c>4 -. }
    \alternative { {
            | % 74
            <gis b>4 -. <gis b>8 -. <gis b>4 -. <gis b>4 -. }
        {
            | % 75
            <gis b>4 -. <gis b>8 -. <gis b>4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Euphonium"
    \set Staff.shortInstrumentName = "Eu."
    \context Staff << 
        \context Voice = "PartPSevenVoiceOne" { \PartPSevenVoiceOne }
        >>
    >>

