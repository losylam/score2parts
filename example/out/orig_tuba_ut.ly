
\version "2.14.2"
% automatically converted from orig_tuba_ut.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        autoBeaming = ##f
        }
    }
PartPNineVoiceOne =  \relative d {
    \clef "bass" \key bes \major \time 7/8 \repeat volta 2 {
        d4. d4 d4 | % 2
        d4. d4 d4 | % 3
        d4. d4 d4 | % 4
        d4. d4 d4 }
    \repeat volta 2 {
        | % 5
        d4. d4 d4 | % 6
        c4. d4 d4 | % 7
        d4. d4 d4 | % 8
        c4. d4 d4 | % 9
        d4. d4 d4 | \barNumberCheck #10
        c4. d4 d4 | % 11
        d4. d4 d4 }
    \alternative { {
            | % 12
            c4 cis8 d4 d4 }
        {
            | % 13
            c4 cis8 d4 fis4 }
        } \repeat volta 2 {
        | % 14
        g4. g4 g4 | % 15
        f4. f4 f4 | % 16
        es4. es4 es4 | % 17
        d4. es4 fis4 | % 18
        g4. g4 g4 | % 19
        f4. f4 f4 | \barNumberCheck #20
        es4. es4 es4 }
    \alternative { {
            | % 21
            d4. d4 fis4 }
        {
            | % 22
            d4. d4 d4 }
        } \repeat volta 2 {
        | % 23
        d4. d4 d4 | % 24
        c4. d4 d4 }
    \repeat volta 2 {
        | % 25
        d4. d4 d4 | % 26
        c4. d4 d4 | % 27
        d4. d4 d4 | % 28
        c4. d4 d4 | % 29
        d4. d4 d4 | \barNumberCheck #30
        c4. d4 d4 }
    \alternative { {
            | % 31
            d4. d4 d4 | % 32
            c4 cis8 d4 d4 }
        {
            | % 33
            c4 -. r8 r2 | % 34
            r8 r2 fis4 }
        } \repeat volta 2 {
        | % 35
        g4. g4 g4 | % 36
        f4. f4 f4 | % 37
        es4. es4 es4 | % 38
        d4. es4 fis4 | % 39
        g4. g4 g4 | \barNumberCheck #40
        f4. f4 f4 | % 41
        es4. es4 es4 }
    \alternative { {
            | % 42
            d4. d4 fis4 }
        {
            | % 43
            d4. d4 d4 }
        } \repeat volta 2 {
        | % 44
        d4 d8 fis4 a4 | % 45
        es4. c4 c8 [ cis8 ] | % 46
        d4 d8 fis4 a4 }
    \alternative { {
            | % 47
            c8 [ bes8 a8 g8 fis8 es8 c8 ] }
        {
            | % 48
            c'8 [ bes8 a8 g8 fis8 es8 c8 ] }
        } \repeat volta 2 {
        | % 49
        d4. d4 -. d4 -. | \barNumberCheck #50
        c4. c4 -. c4 -. | % 51
        d4. d4 -. d4 -. | % 52
        c4. c4 -. c4 -. }
    \repeat volta 2 {
        | % 53
        d4. d4 d4 | % 54
        d4. d4 d4 | % 55
        d4. d4 d4 | % 56
        d4 a8 bes4 cis4 }
    \repeat volta 2 {
        | % 57
        d4. d4 d4 | % 58
        c4. d4 d4 | % 59
        d4. d4 d4 | \barNumberCheck #60
        c4. d4 d4 | % 61
        d4. d4 d4 | % 62
        c4. d4 d4 | % 63
        d4. d4 d4 }
    \alternative { {
            | % 64
            c4 cis8 d4 d4 }
        {
            | % 65
            c4 cis8 d4 fis4 }
        } \repeat volta 2 {
        | % 66
        g4. g4 g4 | % 67
        f4. f4 f4 | % 68
        es4. es4 es4 }
    \alternative { {
            | % 69
            d4. es4 fis4 }
        {
            | \barNumberCheck #70
            d4. d4 fis4 }
        } \repeat volta 2 {
        | % 71
        g4. g4 g4 | % 72
        f4. f4 f4 | % 73
        es4. es4 es4 }
    \alternative { {
            | % 74
            d4. es4 fis4 }
        {
            | % 75
            d4. d4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Tuba Ut"
    \set Staff.shortInstrumentName = "Tu. Ut"
    \context Staff << 
        \context Voice = "PartPNineVoiceOne" { \PartPNineVoiceOne }
        >>
    >>

