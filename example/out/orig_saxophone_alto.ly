
\version "2.14.2"
% automatically converted from orig_saxophone_alto.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPThreeVoiceOne =  \relative dis'' {
    \transposition es' \clef "treble" \key g \major \time 7/8 \repeat
    volta 2 {
        R2..*4 }
    \repeat volta 2 {
        | % 5
        dis8 [ e8 dis8 ] fis4 e8 [ dis8 ] | % 6
        c8 [ a8 c8 ] b4 b4 | % 7
        dis8 [ e8 dis8 ] fis4 a8 [ g8 ] | % 8
        fis8 [ e8 g8 ] fis4 fis4 | % 9
        dis8 [ e8 dis8 ] fis4 e8 [ dis8 ] | \barNumberCheck #10
        c8 [ a8 c8 ] b4 b4 | % 11
        dis8 ( -> [ e8 dis8 ) fis8 ( -> e8 ) dis8 ( -> c8 ) ] }
    \alternative { {
            | % 12
            b8 [ dis8 c8 ] b2 }
        {
            | % 13
            e8 [ dis8 c8 ] b4 dis4 }
        } \repeat volta 2 {
        | % 14
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 15
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | % 16
        c8 [ dis8 e8 g8 fis8 e8 c8 ] | % 17
        b8 [ c8 dis8 ] fis4 g8 [ fis8 ] | % 18
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 19
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | \barNumberCheck #20
        c8 [ dis8 e8 g8 fis8 e8 c8 ] }
    \alternative { {
            | % 21
            b4. ~ b4 dis4 }
        {
            | % 22
            b4. ~ b2 }
        } \repeat volta 2 {
        | % 23
        R2..*2 }
    \repeat volta 2 {
        | % 25
        a'8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 26
        e8 [ fis8 e8 ] dis4 c8 [ b8 ] | % 27
        c8 [ dis8 e8 fis8 g8 ais8 b8 ] | % 28
        c8 [ a8 c8 ] b4 b4 | % 29
        a8 [ g8 fis8 ] e4 g8 [ fis8 ] | \barNumberCheck #30
        e8 [ fis8 e8 ] dis4 c8 [ b8 ] }
    \alternative { {
            | % 31
            c8 [ dis8 e8 fis8 g8 fis8 e8 ] | % 32
            c8 [ dis8 c8 ] b2 }
        {
            | % 33
            c8 [ dis8 e8 fis8 g8 fis8 e8 ] | % 34
            c8 [ dis8 c8 ] b4 dis4 }
        } \repeat volta 2 {
        | % 35
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 36
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | % 37
        c8 [ dis8 e8 g8 fis8 e8 c8 ] | % 38
        b8 [ c8 dis8 ] fis4 g8 [ fis8 ] | % 39
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | \barNumberCheck #40
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | % 41
        c8 [ dis8 e8 g8 fis8 e8 c8 ] }
    \alternative { {
            | % 42
            b4. ~ b4 dis4 }
        {
            | % 43
            b4. ~ b2 }
        } \repeat volta 2 {
        | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            R2.. }
        } \repeat volta 2 {
        | % 49
        R2..*4 }
    \repeat volta 2 {
        | % 53
        R2..*4 }
    \repeat volta 2 {
        | % 57
        dis8 [ e8 dis8 ] fis4 e8 [ dis8 ] | % 58
        c8 [ a8 c8 ] b4 b4 | % 59
        dis8 [ e8 dis8 ] fis4 a8 [ g8 ] | \barNumberCheck #60
        fis8 [ e8 g8 ] fis4 fis4 | % 61
        dis8 [ e8 dis8 ] fis4 e8 [ dis8 ] | % 62
        c8 [ a8 c8 ] b4 b4 | % 63
        dis8 [ e8 dis8 fis8 e8 dis8 c8 ] }
    \alternative { {
            | % 64
            b8 [ dis8 c8 ] b2 }
        {
            | % 65
            e8 [ dis8 c8 ] b4 dis4 }
        } \repeat volta 2 {
        | % 66
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 67
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | % 68
        c8 [ dis8 e8 g8 fis8 e8 c8 ] }
    \alternative { {
            | % 69
            b8 [ c8 dis8 ] fis4 g8 [ fis8 ] }
        {
            | \barNumberCheck #70
            b,4. ~ b4 dis4 }
        } \repeat volta 2 {
        | % 71
        e8 [ g8 fis8 ] e4 g8 [ fis8 ] | % 72
        d8 [ fis8 e8 ] d4 e8 [ d8 ] | % 73
        c8 [ dis8 e8 g8 fis8 e8 c8 ] }
    \alternative { {
            | % 74
            b8 [ c8 dis8 ] fis4 g8 [ fis8 ] }
        {
            | % 75
            b,4. b4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Saxophone Alto"
    \set Staff.shortInstrumentName = "Sax. A."
    \context Staff << 
        \context Voice = "PartPThreeVoiceOne" { \PartPThreeVoiceOne }
        >>
    >>

