
\version "2.14.2"
% automatically converted from orig_tuba_b..xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        autoBeaming = ##f
        }
    }
PartPOneZeroVoiceOne =  \relative e' {
    \transposition bes, \key c \major \time 7/8 \repeat volta 2 {
        e4. e4 e4 | % 2
        e4. e4 e4 | % 3
        e4. e4 e4 | % 4
        e4. e4 e4 }
    \repeat volta 2 {
        | % 5
        e4. e4 e4 | % 6
        d4. e4 e4 | % 7
        e4. e4 e4 | % 8
        d4. e4 e4 | % 9
        e4. e4 e4 | \barNumberCheck #10
        d4. e4 e4 | % 11
        e4. e4 e4 }
    \alternative { {
            | % 12
            d4 dis8 e4 e4 }
        {
            | % 13
            d4 dis8 e4 gis4 }
        } \repeat volta 2 {
        | % 14
        a4. a4 a4 | % 15
        g4. g4 g4 | % 16
        f4. f4 f4 | % 17
        e4. f4 gis4 | % 18
        a4. a4 a4 | % 19
        g4. g4 g4 | \barNumberCheck #20
        f4. f4 f4 }
    \alternative { {
            | % 21
            e4. e4 gis4 }
        {
            | % 22
            e4. e4 e4 }
        } \repeat volta 2 {
        | % 23
        e4. e4 e4 | % 24
        d4. e4 e4 }
    \repeat volta 2 {
        | % 25
        e4. e4 e4 | % 26
        d4. e4 e4 | % 27
        e4. e4 e4 | % 28
        d4. e4 e4 | % 29
        e4. e4 e4 | \barNumberCheck #30
        d4. e4 e4 }
    \alternative { {
            | % 31
            e4. e4 e4 | % 32
            d4 es8 e4 e4 }
        {
            | % 33
            d4 -. r8 r2 | % 34
            r8 r2 gis4 }
        } \repeat volta 2 {
        | % 35
        a4. a4 a4 | % 36
        g4. g4 g4 | % 37
        f4. f4 f4 | % 38
        e4. f4 gis4 | % 39
        a4. a4 a4 | \barNumberCheck #40
        g4. g4 g4 | % 41
        f4. f4 f4 }
    \alternative { {
            | % 42
            e4. e4 gis4 }
        {
            | % 43
            e4. e4 e4 }
        } \repeat volta 2 {
        | % 44
        e4 e8 gis4 b4 | % 45
        f4. d4 d8 [ dis8 ] | % 46
        e4 e8 gis4 b4 }
    \alternative { {
            | % 47
            d8 [ c8 b8 a8 gis8 f8 d8 ] }
        {
            | % 48
            d'8 [ c8 b8 a8 gis8 f8 d8 ] }
        } \repeat volta 2 {
        | % 49
        e4. e4 -. e4 -. | \barNumberCheck #50
        d4. d4 -. d4 -. | % 51
        e4. e4 -. e4 -. | % 52
        d4. d4 -. d4 -. }
    \repeat volta 2 {
        | % 53
        e4. e4 e4 | % 54
        e4. e4 e4 | % 55
        e4. e4 e4 | % 56
        e4 b8 c4 dis4 }
    \repeat volta 2 {
        | % 57
        e4. e4 e4 | % 58
        d4. e4 e4 | % 59
        e4. e4 e4 | \barNumberCheck #60
        d4. e4 e4 | % 61
        e4. e4 e4 | % 62
        d4. e4 e4 | % 63
        e4. e4 e4 }
    \alternative { {
            | % 64
            d4 dis8 e4 e4 }
        {
            | % 65
            d4 dis8 e4 gis4 }
        } \repeat volta 2 {
        | % 66
        a4. a4 a4 | % 67
        g4. g4 g4 | % 68
        f4. f4 f4 }
    \alternative { {
            | % 69
            e4. f4 gis4 }
        {
            | \barNumberCheck #70
            e4. e4 gis4 }
        } \repeat volta 2 {
        | % 71
        a4. a4 a4 | % 72
        g4. g4 g4 | % 73
        f4. f4 f4 }
    \alternative { {
            | % 74
            e4. f4 gis4 }
        {
            | % 75
            e4. e4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Tuba B."
    \set Staff.shortInstrumentName = "Tu. B"
    \context Staff << 
        \context Voice = "PartPOneZeroVoiceOne" { \PartPOneZeroVoiceOne }
        >>
    >>

