
\version "2.14.2"
% automatically converted from orig_saxophone_baryton.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        autoBeaming = ##f
        }
    }
PartPEightVoiceOne =  \relative b' {
    \transposition es, \clef "treble" \key g \major \time 7/8 \repeat
    volta 2 {
        b4. b4 b4 | % 2
        b4. b4 b4 | % 3
        b4. b4 b4 | % 4
        b4. b4 b4 }
    \repeat volta 2 {
        | % 5
        b4. b4 b4 | % 6
        a4. b4 b4 | % 7
        b4. b4 b4 | % 8
        a4. b4 b4 | % 9
        b4. b4 b4 | \barNumberCheck #10
        a4. b4 b4 | % 11
        b4. b4 b4 }
    \alternative { {
            | % 12
            a4 ais8 b4 b4 }
        {
            | % 13
            a4 ais8 b4 dis4 }
        } \repeat volta 2 {
        | % 14
        e4. e4 e4 | % 15
        d4. d4 d4 | % 16
        c4. c4 c4 | % 17
        b4. c4 dis4 | % 18
        e4. e4 e4 | % 19
        d4. d4 d4 | \barNumberCheck #20
        c4. c4 c4 }
    \alternative { {
            | % 21
            b4. b4 dis4 }
        {
            | % 22
            b4. b4 b4 }
        } \repeat volta 2 {
        | % 23
        b4. b4 b4 | % 24
        a4. b4 b4 }
    \repeat volta 2 {
        | % 25
        b4. b4 b4 | % 26
        a4. b4 b4 | % 27
        b4. b4 b4 | % 28
        a4. b4 b4 | % 29
        b4. b4 b4 | \barNumberCheck #30
        a4. b4 b4 }
    \alternative { {
            | % 31
            b4. b4 b4 | % 32
            a4 ais8 b4 b4 }
        {
            | % 33
            a4 -. r8 r2 | % 34
            r8 r2 dis4 }
        } \repeat volta 2 {
        | % 35
        e4. e4 e4 | % 36
        d4. d4 d4 | % 37
        c4. c4 c4 | % 38
        b4. c4 dis4 | % 39
        e4. e4 e4 | \barNumberCheck #40
        d4. d4 d4 | % 41
        c4. c4 c4 }
    \alternative { {
            | % 42
            b4. b4 dis4 }
        {
            | % 43
            b4. b4 b4 }
        } \repeat volta 2 {
        | % 44
        b4 b8 dis4 fis4 | % 45
        c4. a4 a8 [ ais8 ] | % 46
        b4 b8 dis4 fis4 }
    \alternative { {
            | % 47
            a8 [ g8 fis8 e8 dis8 c8 a8 ] }
        {
            | % 48
            a'8 [ g8 fis8 e8 dis8 c8 a8 ] }
        } \repeat volta 2 {
        | % 49
        b4. b4 -. b4 -. | \barNumberCheck #50
        a4. a4 -. a4 -. | % 51
        b4. b4 -. b4 -. | % 52
        a4. a4 -. a4 -. }
    \repeat volta 2 {
        | % 53
        b4. b4 b4 | % 54
        b4. b4 b4 | % 55
        b4. b4 b4 | % 56
        b4 fis8 g4 ais4 }
    \repeat volta 2 {
        | % 57
        b4. b4 b4 | % 58
        a4. b4 b4 | % 59
        b4. b4 b4 | \barNumberCheck #60
        a4. b4 b4 | % 61
        b4. b4 b4 | % 62
        a4. b4 b4 | % 63
        b4. b4 b4 }
    \alternative { {
            | % 64
            a4 ais8 b4 b4 }
        {
            | % 65
            a4 ais8 b4 dis4 }
        } \repeat volta 2 {
        | % 66
        e4. e4 e4 | % 67
        d4. d4 d4 | % 68
        c4. c4 c4 }
    \alternative { {
            | % 69
            b4. c4 dis4 }
        {
            | \barNumberCheck #70
            b4. b4 dis4 }
        } \repeat volta 2 {
        | % 71
        e4. e4 e4 | % 72
        d4. d4 d4 | % 73
        c4. c4 c4 }
    \alternative { {
            | % 74
            b4. c4 dis4 }
        {
            | % 75
            b4. b4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Saxophone Baryton"
    \set Staff.shortInstrumentName = "Sax. Bar."
    \context Staff << 
        \context Voice = "PartPEightVoiceOne" { \PartPEightVoiceOne }
        >>
    >>

