
\version "2.14.2"
% automatically converted from orig_rythmique.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        autoBeaming = ##f
        }
    }
PartPOneOneVoiceOne =  \relative d' {
    \clef "treble" \key bes \major \time 7/8 \repeat volta 2 {
        | % 1
        <d fis a>4 ^"x1 Tacet" <d fis a>8 <d fis a>4 <d fis a>4 | % 2
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 3
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 4
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
    \repeat volta 2 {
        | % 5
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 6
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 7
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 8
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 9
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | \barNumberCheck
        #10
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 11
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
    \alternative { {
            | % 12
            <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 }
        {
            | % 13
            <c es g>4 <c es g>8 <d fis a>4 <d fis a c>4 }
        } \repeat volta 2 {
        | % 14
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | % 15
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | % 16
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 | % 17
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 | % 18
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | % 19
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | \barNumberCheck #20
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 }
    \alternative { {
            | % 21
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 }
        {
            | % 22
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
        } \repeat volta 2 {
        | % 23
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 24
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 }
    \repeat volta 2 {
        | % 25
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 26
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 27
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 28
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 29
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | \barNumberCheck
        #30
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 }
    \alternative { {
            | % 31
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 32
            <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 }
        {
            | % 33
            <c es g>4 -. r2 r8 | % 34
            r2 r8 <d ges a c>4 }
        } \repeat volta 2 {
        | % 35
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | % 36
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | % 37
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 | % 38
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 | % 39
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | \barNumberCheck
        #40
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | % 41
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 }
    \alternative { {
            | % 42
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 }
        {
            | % 43
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
        } \repeat volta 2 {
        | % 44
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 45
        <c es g>4 <c es g>8 <c es g>4 <c es g>4 | % 46
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
    \alternative { {
            | % 47
            <c es g>4. <c es g>4 <c es g>4 }
        {
            | % 48
            <c es g>4. <c es g>4 <c es g>4 }
        } \repeat volta 2 {
        | % 49
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | \barNumberCheck
        #50
        <c es g>4 <c es g>8 <c es g>4 <c es g>4 | % 51
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 52
        <c es g>4. <c es g>4 <c es g>4 }
    \repeat volta 2 {
        | % 53
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 54
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 55
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 56
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
    \repeat volta 2 {
        | % 57
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 58
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 59
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | \barNumberCheck
        #60
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 61
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 | % 62
        <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 | % 63
        <d fis a>4 <d fis a>8 <d fis a>4 <d fis a>4 }
    \alternative { {
            | % 64
            <c es g>4 <c es g>8 <d fis a>4 <d fis a>4 }
        {
            | % 65
            <c es g>4 <c es g>8 <d fis a>4 <d fis a c>4 }
        } \repeat volta 2 {
        | % 66
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | % 67
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | % 68
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 }
    \alternative { {
            | % 69
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 }
        {
            | \barNumberCheck #70
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 }
        } \repeat volta 2 {
        | % 71
        <g bes d>4 <g bes d>8 <g bes d>4 <g bes d>4 | % 72
        <f a c>4 <f a c>8 <f a c>4 <f a c>4 | % 73
        <es g bes>4 <es g bes>8 <es g bes>4 <es g bes>4 }
    \alternative { {
            | % 74
            <d fis a>4 <d fis a>8 <d fis a>4 <d fis a c>4 }
        {
            | % 75
            <d fis a>4 <d fis a>8 <d fis a>4 -. -> r4 }
        } }

PartPOneOneVoiceOneChords =  \chordmode {
    \repeat volta 2 {
        | % 1
        d4:7 s8 s4 s4 | % 2
        d4:7 s8 s4 s4 | % 3
        d4:7 s8 s4 s4 | % 4
        d4:7 s8 s4 s4 }
    \repeat volta 2 {
        | % 5
        d4:7 s8 s4 s4 | % 6
        c4:m5 s8 d4:7 s4 | % 7
        d4:7 s8 s4 s4 | % 8
        c4:m5 s8 d4:7 s4 | % 9
        d4:7 s8 s4 s4 | \barNumberCheck #10
        c4:m5 c4:m5 d4:7 d4:7 | % 11
        d4:7 s4 s4 }
    \alternative { {
            | % 12
            c4:m5 c4:m5 d4:7 d4:7 }
        {
            | % 13
            c4:m5 d4:5 d4:7 }
        } \repeat volta 2 {
        | % 14
        g4:m5 s8 s4 s4 | % 15
        f4:5 s8 s4 s4 | % 16
        es4:5 s8 s4 s4 | % 17
        d4:5 s8 s4 d4:7 | % 18
        g4:m5 s8 s4 s4 | % 19
        f4:5 s8 s4 s4 | \barNumberCheck #20
        es4:5 s8 s4 s4 }
    \alternative { {
            | % 21
            d4:5 d4:5 s8 d4:7 d4:7 }
        {
            | % 22
            d4:7 s8 s4 }
        } \repeat volta 2 {
        | % 23
        s4 s8 s4 s4 | % 24
        c4:m5 s8 d4:7 s4 }
    \repeat volta 2 {
        | % 25
        s4 s8 s4 s4 | % 26
        c4:m5 s8 d4:7 s4 | % 27
        s4 s8 s4 s4 | % 28
        c4:m5 c4:m5 d4:7 d4:7 | % 29
        s8 s8 s4 s4 | \barNumberCheck #30
        c4:m5 c4:m5 d4:7 d4:7 }
    \alternative { {
            | % 31
            s8 s8 s4 s4 | % 32
            c4:m5 c4:m5 c4:m5 d4:7 d4:7 d4:7 }
        {
            | % 33
            c4:m5 | % 34
            s2 s8 d4:7 }
        } \repeat volta 2 {
        | % 35
        g4:m5 s8 s4 s4 | % 36
        f4:5 s8 s4 s4 | % 37
        es4:5 s8 s4 s4 | % 38
        d4:5 s8 s4 d4:7 | % 39
        g4:m5 s8 s4 s4 | \barNumberCheck #40
        f4:5 s8 s4 s4 | % 41
        es4:5 s8 s4 s4 }
    \alternative { {
            | % 42
            d4:5 s8 s4 d4:7 }
        {
            | % 43
            d4:7 s8 s4 s4 }
        } \repeat volta 2 {
        | % 44
        d4:7 d4:7 s8 s4 | % 45
        c4:m5 s8 s4 s4 | % 46
        d4:7 d4:7 s8 s4 }
    \alternative { {
            | % 47
            c4.:m5 s4 s4 }
        {
            | % 48
            c4.:m5 s4 s4 }
        } \repeat volta 2 {
        | % 49
        d4:7 d4:7 s8 s4 | \barNumberCheck #50
        c4:m5 s8 s4 s4 | % 51
        d4:7 d4:7 s8 s4 | % 52
        c4.:m5 s4 s4 }
    \repeat volta 2 {
        | % 53
        d4:7 d4:7 s8 s4 | % 54
        d4:7 d4:7 s8 s4 | % 55
        d4:7 d4:7 s8 s4 | % 56
        d4:7 d4:7 s8 s4 }
    \repeat volta 2 {
        | % 57
        d4:7 d4:7 s8 s4 | % 58
        c4:m5 s8 d4:7 s4 | % 59
        d4:7 d4:7 s8 s4 | \barNumberCheck #60
        c4:m5 s8 d4:7 s4 | % 61
        d4:7 d4:7 s8 s4 | % 62
        c4:m5 s8 d4:7 s4 | % 63
        d4:7 d4:7 s8 s4 }
    \alternative { {
            | % 64
            c4:m5 s8 d4:7 s4 }
        {
            | % 65
            c4:m5 s8 d4:5 d4:7 }
        } \repeat volta 2 {
        | % 66
        g4:m5 s8 s4 s4 | % 67
        f4:5 s8 s4 s4 | % 68
        es4:5 s8 s4 s4 }
    \alternative { {
            | % 69
            d4:5 s8 s4 d4:7 }
        {
            | \barNumberCheck #70
            d4:7 d4:5 s8 d4:7 }
        } \repeat volta 2 {
        | % 71
        g4:m5 s8 s4 s4 | % 72
        f4:5 s8 s4 s4 | % 73
        es4:5 s8 s4 s4 }
    \alternative { {
            | % 74
            d4:5 d4:5 s8 d4:7 d4:7 }
        {
            | % 75
            d4:7 s8 s4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Rythmique"
    \set Staff.shortInstrumentName = "Ryt."
    \context ChordNames = "PartPOneOneVoiceOneChords" \PartPOneOneVoiceOneChords
    \context Staff <<
        \context Voice = "PartPOneOneVoiceOne" { \PartPOneOneVoiceOne }
        >>
    >>

