
\version "2.14.2"
% automatically converted from orig_saxophone_tenor.xml

\header {
    encodingsoftware = "MuseScore 1.2"
    tagline = "MuseScore 1.2"
    encodingdate = "2015-04-15"
    }

#(set-global-staff-size 20.0762645669)
\paper {
    paper-width = 21.0\cm
    paper-height = 29.7\cm
    top-margin = 1.0\cm
    bottom-margin = 2.0\cm
    left-margin = 1.0\cm
    right-margin = 1.0\cm
    }
\layout {
    \context { \Score
        skipBars = ##t
        autoBeaming = ##f
        }
    }
PartPFourVoiceOne =  \relative gis' {
    \transposition bes \clef "treble" \key c \major \time 7/8 \repeat
    volta 2 {
        R2..*4 }
    \repeat volta 2 {
        | % 5
        gis8 [ a8 gis8 ] b4 a8 [ gis8 ] | % 6
        f8 [ d8 f8 ] e4 e4 | % 7
        gis8 [ a8 gis8 ] b4 d8 [ c8 ] | % 8
        b8 [ a8 c8 ] b4 b4 | % 9
        gis8 [ a8 gis8 ] b4 a8 [ gis8 ] | \barNumberCheck #10
        f8 [ d8 f8 ] e4 e4 | % 11
        gis8 ( -> [ a8 gis8 ) b8 ( -> a8 ) gis8 ( -> f8 ) ] }
    \alternative { {
            | % 12
            e8 [ gis8 f8 ] e2 }
        {
            | % 13
            a8 [ gis8 f8 ] e4 gis4 }
        } \repeat volta 2 {
        | % 14
        a8 [ c8 b8 ] a4 c8 [ b8 ] | % 15
        g8 [ b8 a8 ] g4 a8 [ g8 ] | % 16
        f8 [ gis8 a8 c8 b8 a8 f8 ] | % 17
        e8 [ f8 gis8 ] b4 c8 [ b8 ] | % 18
        a8 [ c8 b8 ] a4 c8 [ b8 ] | % 19
        g8 [ b8 a8 ] g4 a8 [ g8 ] | \barNumberCheck #20
        f8 [ gis8 a8 c8 b8 a8 f8 ] }
    \alternative { {
            | % 21
            e4. ~ e4 gis4 }
        {
            | % 22
            e4. ~ e2 }
        } \repeat volta 2 {
        | % 23
        R2..*2 }
    \repeat volta 2 {
        | % 25
        d'8 [ c8 b8 ] a4 c8 [ b8 ] | % 26
        a8 [ b8 a8 ] gis4 f8 [ e8 ] | % 27
        f8 [ gis8 a8 b8 c8 dis8 e8 ] | % 28
        f8 [ d8 f8 ] e4 e4 | % 29
        d8 [ c8 b8 ] a4 c8 [ b8 ] | \barNumberCheck #30
        a8 [ b8 a8 ] gis4 f8 [ e8 ] }
    \alternative { {
            | % 31
            f8 [ gis8 a8 b8 c8 b8 a8 ] | % 32
            f8 [ gis8 f8 ] e2 }
        {
            | % 33
            f8 [ gis8 a8 b8 c8 b8 a8 ] | % 34
            f8 [ gis8 f8 ] e4 gis4 }
        } \repeat volta 2 {
        | % 35
        a8 [ c8 b8 ] a4 c8 [ b8 ] | % 36
        g8 [ b8 a8 ] g4 a8 [ g8 ] | % 37
        f8 [ gis8 a8 c8 b8 a8 f8 ] | % 38
        e8 [ f8 gis8 ] b4 c8 [ b8 ] | % 39
        a8 [ c8 b8 ] a4 c8 [ b8 ] | \barNumberCheck #40
        g8 [ b8 a8 ] g4 a8 [ g8 ] | % 41
        f8 [ gis8 a8 c8 b8 a8 f8 ] }
    \alternative { {
            | % 42
            e4. ~ e4 gis4 }
        {
            | % 43
            e4. ~ e2 }
        } \repeat volta 2 {
        | % 44
        R2..*3 }
    \alternative { {
            | % 47
            R2.. }
        {
            | % 48
            R2.. }
        } \repeat volta 2 {
        | % 49
        R2..*4 }
    \repeat volta 2 {
        | % 53
        R2..*4 }
    \repeat volta 2 {
        | % 57
        gis8 [ a8 gis8 ] b4 a8 [ gis8 ] | % 58
        f8 [ d8 f8 ] e4 e4 | % 59
        gis8 [ a8 gis8 ] b4 d8 [ c8 ] | \barNumberCheck #60
        b8 [ a8 c8 ] b4 b4 | % 61
        gis8 [ a8 gis8 ] b4 a8 [ gis8 ] | % 62
        f8 [ d8 f8 ] e4 e4 | % 63
        gis8 [ a8 gis8 b8 a8 gis8 f8 ] }
    \alternative { {
            | % 64
            e8 [ gis8 f8 ] e2 }
        {
            | % 65
            a8 [ gis8 f8 ] e4 gis4 }
        } \repeat volta 2 {
        | % 66
        a8 [ c8 b8 ] a4 c8 [ b8 ] | % 67
        g8 [ b8 a8 ] g4 a8 [ g8 ] | % 68
        f8 [ gis8 a8 c8 b8 a8 f8 ] }
    \alternative { {
            | % 69
            e8 [ f8 gis8 ] b4 c8 [ b8 ] }
        {
            | \barNumberCheck #70
            e,4. ~ e4 gis4 }
        } \repeat volta 2 {
        | % 71
        a8 [ c8 b8 ] a4 c8 [ b8 ] | % 72
        g8 [ b8 a8 ] g4 a8 [ g8 ] | % 73
        f8 [ gis8 a8 c8 b8 a8 f8 ] }
    \alternative { {
            | % 74
            e8 [ f8 gis8 ] b4 c8 [ b8 ] }
        {
            | % 75
            e,4. e4 -. -> r4 }
        } }


% The score definition
\new Staff <<
    \set Staff.instrumentName = "Saxophone Tenor"
    \set Staff.shortInstrumentName = "Sax. T."
    \context Staff << 
        \context Voice = "PartPFourVoiceOne" { \PartPFourVoiceOne }
        >>
    >>

