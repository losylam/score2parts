#!/env/python

import subprocess
from xml.dom import minidom
import sys
import os

def mscConvert(fichier_entree, fichier_sortie):
    """
    convert a file with the terminal user interface of musescore
    file formats are defined by extension
    """

    com = ['musescore',
           fichier_entree,
           '-o',
           fichier_sortie]
    return subprocess.call(com)

def xml2lilypdf(fichier_entree):
    """
    convert a musicxml file to pdf using musicxml2ly and lilypond

    """
    if fichier_entree[-4:] != '.xml':
        print "le fichier n'a pas d'extension .xml"
        return None
    else:
        nom = fichier_entree[:-4]
        nom_ly = nom+'.ly'
        nom_pdf = nom+'.pdf'
    subprocess.call(['musicxml2ly', '-o', nom_ly, fichier_entree])
    subprocess.call(['lilypond', '-o', nom, nom_ly])

class Score2Parts:    
    def __init__(self, fichier_entree, export = False, export_lilypdf = False):
        """
        split a score file musicxml into a part musicxml by instrument
        
        plus :
        * convert the input file if not xml
        * export directly if export == True
        """
        self.fichier_entree = fichier_entree
        self.ext = fichier_entree.split('.')[-1]
        self.nom = fichier_entree[:-(len(self.ext)+1)]
        
        self.export_lilypdf = export_lilypdf

        if self.ext == 'xml':
            self.fichier_xml = self.fichier_entree
        else:
            self.fichier_xml = '%s.xml'%self.nom
            mscConvert(self.fichier_entree, self.fichier_xml)

        self.xml = minidom.parse(self.fichier_xml)
        self.removeNewPage()

        # get containers
        score_partwise = self.xml.getElementsByTagName('score-partwise')[0]
        part_list = self.xml.getElementsByTagName('part-list')[0]
        
        self.score_partwise = score_partwise
        self.part_list = part_list

        # get elements and remove from DOM
        self.parts = [score_partwise.removeChild(p) for p in score_partwise.getElementsByTagName('part')]
        self.score_parts = [part_list.removeChild(sp) for sp in part_list.getElementsByTagName('score-part')]
        self.part_groups = [part_list.removeChild(pg) for pg in part_list.getElementsByTagName('part-group')]

        self.ids = [p.getAttribute('id') for p in self.parts]

        if export:
            self.exportParts()

    def removeNewPage(self):
        """
        delete "new-page" attributes (= saut de page)
        """
        prints = self.xml.getElementsByTagName('print')
        for p in prints:
            if p.hasAttribute('new-page'):
                p.removeAttribute('new-page')

    def getById(self, lst, id_name):
        for i in lst:
            if i.getAttribute('id') == id_name:
                return i
        return None

    def exportPart(self, id_name, nom_fichier = None):
        part = self.getById(self.parts, id_name)
        score_part = self.getById(self.score_parts, id_name)
        
        part_name_xml = score_part.getElementsByTagName('part-name')[0]
        part_name = part_name_xml.firstChild.data
        part_name = part_name.lower().replace(' ', '_')

        if not nom_fichier:
            nom_fichier = '%s_%s.xml'%(self.nom, part_name)

        self.score_partwise.appendChild(part)
        self.part_list.appendChild(score_part)

        f_out = open(nom_fichier, 'w')
        f_out.write(self.xml.toxml().encode('utf-8'))
        f_out.close()

        if self.export_lilypdf:
            xml2lilypdf(nom_fichier)

        self.score_partwise.removeChild(part)
        self.part_list.removeChild(score_part)
        
    def exportParts(self, nom_fichier = None):
        for i in self.ids:
            self.exportPart(i, nom_fichier = nom_fichier)
        
if __name__=='__main__':
    if len(sys.argv) >= 2:
        nom = sys.argv[1]
        s2p = Score2Parts(nom, export = True, export_lilypdf = True)
