Score2parts
================

Description
----------------

Utilitaire python pour manipuler score et partitions au format musicxml, musescore et lilypond

Dépendances :
* musescore pour conversion vers xml et pdf avec musescore
* musicxml2ly et lilypond pour export lilypond


Mode d'emploi
----------------

Dans un terminal :
$ python score2parts.py [INPUT]

Avec [INPUT] un fichier au format musicxml ou n'importe quel format supporté par musescore
